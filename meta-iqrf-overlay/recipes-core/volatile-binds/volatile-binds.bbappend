VOLATILE_BINDS = "/data/etc/iqrf-gateway.json /etc/iqrf-gateway.json\n\
	/data/etc/iqrf-gateway-daemon /etc/iqrf-gateway-daemon\n\
	/data/etc/iqrf-gateway-daemon/certs/core /etc/iqrf-gateway-daemon/certs/core\n\
	/data/etc/iqrf-gateway-webapp /etc/iqrf-gateway-webapp\n\
	/data/etc/iqrf-gateway-controller /etc/iqrf-gateway-controller\n\
	/data/etc/iqrf-gateway-uploader /etc/iqrf-gateway-uploader\n\
	/data/etc/nginx /etc/nginx\n\
	/data/etc/mender /etc/mender\n\
	/data/etc/monit /etc/monit\n\
	/data/etc/hostname /etc/hostname\n\
	/data/etc/NetworkManager /etc/NetworkManager\n\
	/data/etc/hosts /etc/hosts\n\
	/data/etc/ssh /etc/ssh\n\
	/data/usr/share/iqrf-gateway /usr/share/iqrf-gateway\n\
	/data/usr/share/iqrf-gateway-daemon /usr/share/iqrf-gateway-daemon\n\
	/data/usr/share/iqrf-gateway-daemon/DB /usr/share/iqrf-gateway-daemon/DB\n\
	/data/var/cache/iqrf-gateway-daemon/iqrfRepoCache /var/cache/iqrf-gateway-daemon/iqrfRepoCache\n\
	/data/var/cache/iqrf-gateway-daemon/iqrfRepoCache/cache /var/cache/iqrf-gateway-daemon/iqrfRepoCache/cache\n\
	/data/var/cache/iqrf-gateway-daemon/scheduler /var/cache/iqrf-gateway-daemon/scheduler\n\
	/data/var/cache/iqrf-gateway-daemon/upload /var/cache/iqrf-gateway-daemon/upload\n\
	/data/usr/share/iqrf-cloud-provisioning/cert /usr/share/iqrf-cloud-provisioning/cert\n\
	/data/var/lib /var/lib\n\
	/data/var/lib/iqrf-gateway-webapp /var/lib/iqrf-gateway-webapp\n\
	/data/var/cache /var/cache\n\
	/data/var/cache/iqrf-gateway-webapp/cache /var/cache/iqrf-gateway-webapp/cache\n\
	/data/var/cache/iqrf-gateway-webapp/sessions /var/cache/iqrf-gateway-webapp/sessions\n\
	/data/var/spool /var/spool\n\
	/data/home/root /home/root\n\
	/var/volatile/srv /srv\n\
"

FILES:${PN} += " /data/*"

do_install:append() {
	install -d ${D}/data/etc
	install -d ${D}/data/etc/iqrf-gateway-daemon/certs

	install -d ${D}/data/usr/share
	install -d ${D}/data/usr/share/iqrf-gateway
	install -d ${D}/data/usr/share/iqrf-gateway-daemon

	install -d ${D}/data/var/cache/iqrf-gateway-daemon
	install -d ${D}/data/var/cache/iqrf-gateway-daemon/iqrfRepoCache

	install -d ${D}/data/usr/share/iqrf-cloud-provisioning/cert

	install -d ${D}/data/var
	install -d ${D}/data/var/lib/iqrf-gateway-webapp

	install -d ${D}/data/home
}
