LICENSE = "GPLv2"
DESCRIPTION = "Report the historical and statistical real time of the system, keeping it between restarts. Like uptime command but with more interesting output."
HOMEPAGE = "https://github.com/rfmoz/tuptime"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

inherit systemd useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "--system --no-create-home \
		       --home-dir '/var/lib/tuptime' \
                       --user-group ${PN}"

SRC_URI = "file://tuptime \
		file://systemd/ \
		file://cron.d/  \
"

FILES:${PN} += "${bindir}/* ${sysconfdir}/* ${systemd_system_unitdir}/* "

do_install() {
	install -d ${D}${bindir}
	install -m 755 ${WORKDIR}/tuptime ${D}${bindir}
	
	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${WORKDIR}/systemd/tuptime.service ${D}${systemd_system_unitdir}

	install -d ${D}${sysconfdir}/cron.d 
	install -m 644 ${WORKDIR}/cron.d/tuptime ${D}${sysconfdir}/cron.d

	install -d ${D}/var/lib/tuptime
	chown -R ${PN}:${PN} ${D}/var/lib/tuptime
}

SYSTEMD_SERVICE:${PN} = "tuptime.service"
SYSTEMD_AUTO_ENABLE = "enable"
