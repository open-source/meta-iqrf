FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://sudo \
"

do_install:append() {
	install -d ${D}${sysconfdir}/bash_completion.d/
	install -m 755 ${WORKDIR}/sudo ${D}${sysconfdir}/bash_completion.d
}
