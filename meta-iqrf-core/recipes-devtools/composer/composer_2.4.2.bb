LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://composer.phar;md5=6e3c84c9c1dfbdb5dcfd849f51f04069"

SRC_URI = "https://getcomposer.org/download/${PV}/composer.phar"

SRC_URI[md5sum] = "6e3c84c9c1dfbdb5dcfd849f51f04069"
SRC_URI[sha256sum] = "8fe98a01050c92cc6812b8ead3bd5b6e0bcdc575ce7a93b242bde497a31d7732"

S="${WORKDIR}"

do_install() {
	install -d ${D}${bindir}
	install -m 755 ${S}/composer.phar ${D}${bindir}/composer
}

BBCLASSEXTEND = "native"
