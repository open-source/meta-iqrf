FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

DEPENDS += "libsodium curl libxml2 libxslt zlib bind icu"

DEPENDS:class-native += "curl-native libsodium-native libxml2-native libxslt-native icu-native"

EXTRA_OECONF:append = " --enable-fpm --enable-intl --with-curl --with-libxml --with-sodium  --with-xsl"

EXTRA_OECONF:class-native = " \
	${COMMON_EXTRA_OECONF} \
"

PACKAGECONFIG = "zip sqlite3 opcache openssl mbstring \
	${@bb.utils.filter('DISTRO_FEATURES', 'ipv6 pam', d)} \
"

PACKAGECONFIG:class-native = "zip sqlite3 opcache openssl mbstring \
	${@bb.utils.filter('DISTRO_FEATURES', 'ipv6 pam', d)} \
"
