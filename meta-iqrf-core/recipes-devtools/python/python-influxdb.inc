SUMMARY = "Python client for InfluxDB."
HOMEPAGE = "https://github.com/influxdata/influxdb-python.git"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f61c0d3948045e1c8ebe3d70cc888003"

inherit pypi

PYPI_PACKAGE = "influxdb"

SRC_URI[md5sum] = "6e1b7c63785720c72b901e2a8c1aca96"
SRC_URI[sha256sum] = "58c647f6043712dd86e9aee12eb4ccfbbb5415467bc9910a48aa8c74c1108970"

RDEPENDS:${PN} += " \
    ${PYTHON_PN}-dateutil \
    ${PYTHON_PN}-msgpack \
    ${PYTHON_PN}-pytz \
    ${PYTHON_PN}-requests \
    ${PYTHON_PN}-six\
    "
