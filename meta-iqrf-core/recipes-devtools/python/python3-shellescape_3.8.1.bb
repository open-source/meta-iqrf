SUMMARY = "Shell escape a string to safely use it as a token in a shell command (backport of cPython shlex.quote for Python versions 2.x & < 3.3)"
HOMEPAGE = "https://github.com/chrissimpkins/shellescape"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://docs/LICENSE;md5=3b50a04c51db24cec83a8f24c1bf24a3"

SRC_URI = "git://github.com/chrissimpkins/shellescape;protocol=https;branch=master"

# Modify these as desired
PV = "3.8.1+git${SRCPV}"
SRCREV = "62c74bb86c6d264ea52acbe7271bb721d5c24bc2"

S = "${WORKDIR}/git"

inherit setuptools3

RDEPENDS:${PN} += "python3-core python3-shell"
