FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " file://crontab \
"

do_install:append() {
	install -d ${D}${sysconfdir}
    install -m 644 ${WORKDIR}/crontab ${D}${sysconfdir}
}

CONFFILES:${PN} += "${sysconfdir}/crontab"
