LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=0636e73ff0215e8d672dc4c32c317bb3 \
                    file://debian/copyright;md5=9ec36f3cb9555a4814545b7455f5a4ad"

SRC_URI = "git://git.einval.com/git/fake-hwclock.git;protocol=https \
	       file://0001-Put-data-file-to-data-partition.patch \
"

SRCREV = "6652b58ba37566f495cb9b52e71d6858842a2265"

S = "${WORKDIR}/git"

RDEPENDS:${PN} += "cronie"

inherit systemd

SYSTEMD_SERVICE:${PN} = "fake-hwclock.service"

do_configure () {
	:
}

do_compile () {
	# Specify compilation commands here
	:
}

do_install () {
	install -d ${D}${sysconfdir}/default/
	install -m 644 ${S}/etc/default/fake-hwclock ${D}${sysconfdir}/default/
	
	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${S}/debian/fake-hwclock.service ${D}${systemd_system_unitdir}

	install -d ${D}/sbin
	install -m 755 ${S}/fake-hwclock ${D}/sbin

	install -d ${D}${sysconfdir}/cron.hourly
	install -m 755 ${S}/debian/fake-hwclock.cron.hourly ${D}${sysconfdir}/cron.hourly/fake-hwclock
}
