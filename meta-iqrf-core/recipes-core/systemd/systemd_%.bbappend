FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = " \
	file://init.sh \
	file://0001-Move-localtime-to-data-partition.patch \
	file://50-iqrf-sysctl.conf \
	file://50-iqrf-journald.conf \
	file://50-iqrf-timesyncd.conf \
	file://wait_for_time_sync.conf \
	file://timeout.conf \
"

do_install:append() {
	install -d ${D}${sbindir}
	install -m 755 ${WORKDIR}/init.sh ${D}${sbindir}

	# we use custom script before running of systemd
	ln -sf /usr/sbin/init.sh ${D}/sbin/init

	# systemd services should have read-write permission to /etc and /data/etc
	sed -i -e "s#\(^ReadWritePaths=.*\)#\1 /data/etc#" ${D}${systemd_unitdir}/system/systemd-timedated.service

	# sysctl conf
	install -m 600 ${WORKDIR}/50-iqrf-sysctl.conf ${D}${sysconfdir}/sysctl.d

	# journal conf
	install -d ${D}/data/lib/systemd/journald.conf.d
	install -m 644 ${WORKDIR}/50-iqrf-journald.conf ${D}/data/lib/systemd/journald.conf.d/

	# timesyncd conf
	install -d ${D}/data/lib/systemd/timesyncd.conf.d
	install -m 644 ${WORKDIR}/50-iqrf-timesyncd.conf ${D}/data/lib/systemd/timesyncd.conf.d/
	install -d ${D}${systemd_unitdir}/timesyncd.conf.d/
	ln -sf /data/lib/systemd/timesyncd.conf.d/50-iqrf-timesyncd.conf ${D}${systemd_unitdir}/timesyncd.conf.d/00-systemd-conf.conf

	# daemon conf
	install -d ${D}${sysconfdir}/systemd/system/iqrf-gateway-daemon.service.d/
	install -m 644 ${WORKDIR}/wait_for_time_sync.conf ${D}${sysconfdir}/systemd/system/iqrf-gateway-daemon.service.d/wait_for_time_sync.conf
	install -d ${D}${sysconfdir}/systemd/system/systemd-time-wait-sync.service.d/
	install -m 644 ${WORKDIR}/timeout.conf ${D}${sysconfdir}/systemd/system/systemd-time-wait-sync.service.d/timeout.conf

	# manually enable service for time sync
    install -d ${D}${sysconfdir}/systemd/system/sysinit.target.wants
    ln -sf ${systemd_unitdir}/system/systemd-time-wait-sync.service \
        ${D}${sysconfdir}/systemd/system/sysinit.target.wants/systemd-time-wait-sync.service

	# bash conf
	install -d ${D}${datadir}/bash-completion/completions
	install -m 644 ${WORKDIR}/git/shell-completion/bash/* ${D}${datadir}/bash-completion/completions/
}

FILES:${PN} += " ${datadir}/* ${sbindir}/* ${systemd_unitdir}/* ${sysconfdir}/systemd/* /data/lib/systemd/journald.conf.d/50-iqrf-journald.conf /data/lib/systemd/timesyncd.conf.d/50-iqrf-timesyncd.conf"
