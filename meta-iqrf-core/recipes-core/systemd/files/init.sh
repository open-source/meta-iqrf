#!/bin/sh

# basic FS mount
mount none -t proc /proc
mount none -t sysfs /sys

loop=0

# wait for ethernet a bit (usb enumeration)
while [ ! -f /sys/class/net/eth0/address ] && [ $loop -le 10 ]; do
	sleep 1
	((loop++))
done

# generate ID form MAC address
MACHINEID=$(cat /sys/class/net/eth0/address | md5sum | cut -d' ' -f1)

# mount data
mount /dev/mmcblk0p6 /data

# mount hostname overlay
/sbin/mount-copybind /data/etc/hostname /etc/hostname

#Run systemd
exec /lib/systemd/systemd "--machine-id=$MACHINEID"
exit 0
