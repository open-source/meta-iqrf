FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

do_install:append () {
	# we supply custom hosts
	rm -rf ${D}${sysconfdir}/hosts
	# we supply custom hostname
	rm -rf ${D}${sysconfdir}/hostname

	# copy for mender root terminal
	install -d ${D}/home/root
	install -m 0755 ${WORKDIR}/share/dot.profile ${D}/home/root/.profile
	install -m 0755 ${WORKDIR}/share/dot.bashrc ${D}/home/root/.bashrc
}
