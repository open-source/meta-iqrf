FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://ssh.service"

do_install:append() {
	rm -rf ${D}${systemd_system_unitdir}/sshd@.service
	rm -rf ${D}${systemd_system_unitdir}/sshd.socket

	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${WORKDIR}/ssh.service ${D}${systemd_system_unitdir}

	install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants
	ln -sf ${systemd_unitdir}/system/sshdgenkeys.service \
                ${D}${sysconfdir}/systemd/system/multi-user.target.wants/sshdgenkeys.service

	# Create config files for read-only rootfs (store it in data)
  install -d ${D}${sysconfdir}/ssh
  install -m 644 ${D}${sysconfdir}/ssh/sshd_config ${D}${sysconfdir}/ssh/sshd_config_readonly
  sed -i '/HostKey/d' ${D}${sysconfdir}/ssh/sshd_config_readonly
	echo "HostKey /data/ssh/ssh_host_rsa_key" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
	echo "HostKey /data/ssh/ssh_host_ecdsa_key" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
	echo "HostKey /data/ssh/ssh_host_ed25519_key" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
}

SYSTEMD_SERVICE:${PN}-sshd = "sshdgenkeys.service"
