FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://options \
"

do_install:append() {
    install -d ${D}${sysconfdir}/ppp
    install -m 644 ${WORKDIR}/options ${D}${sysconfdir}/ppp
}
