FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://NetworkManager.conf \
			file://nmcli-completions \
"

PACKAGECONFIG:append = " modemmanager ppp"

FILES:${PN} += " ${datadir}/* ${sysconfdir}/*"

do_install:append() {
	install -d ${D}${sysconfdir}/NetworkManager
	install -m 600 ${WORKDIR}/NetworkManager.conf ${D}${sysconfdir}/NetworkManager

	install -d ${D}${datadir}/bash-completion/completions
	install -m 644 ${WORKDIR}/nmcli-completions ${D}${datadir}/bash-completion/completions/nmcli
}
