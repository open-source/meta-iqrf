DESCRIPTION = "Wireguard VPN client configuration"
LICENSE = "CLOSED"

# procps is needed to work around the following wireguard error:
# sysctl: invalid option -- 'r'
# iproute2 is needed as busybox ip command does not implement all needed features.
RDEPENDS:${PN} = "wireguard-tools procps iproute2"

SRC_URI = " \
    file://wireguard.init \
    file://wireguard.default \
    file://wg0.conf \
"
PR = "r1"

inherit update-rc.d

INITSCRIPT_NAME = "wireguard"
INITSCRIPT_PARAMS = "defaults"

do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0750 ${WORKDIR}/wireguard.init ${D}${sysconfdir}/init.d/wireguard

    install -d ${D}${sysconfdir}/default
    install -m 0644 ${WORKDIR}/wireguard.default ${D}${sysconfdir}/default/wireguard

    install -d ${D}${sysconfdir}/wireguard
    install -m 0640 ${WORKDIR}/wg0.conf ${D}${sysconfdir}/wireguard/wg0.conf
}

FILES:${PN} = "${sysconfdir}"
