FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://mender-connect.conf"

SYSTEMD_AUTO_ENABLE = "disable"

do_install:append () {
    install -d ${D}${sysconfdir}/mender
    install -m 600 ${WORKDIR}/mender-connect.conf ${D}${sysconfdir}/mender
}
