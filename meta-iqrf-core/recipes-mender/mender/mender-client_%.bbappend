FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://mender-device-identity \
	file://diff-update \
"

PACKAGECONFIG:append = " modules"

SYSTEMD_AUTO_ENABLE = "disable"

RDEPENDS:${PN}:append = " rdiff"

do_install:append() {
	install -d ${D}${datadir}/mender/identity
	install -m 755 ${WORKDIR}/mender-device-identity ${D}${datadir}/mender/identity

	install -d ${D}${datadir}/mender/modules/v3
	install -m 755 ${WORKDIR}/diff-update ${D}${datadir}/mender/modules/v3
}

FILES:${PN}:append = " ${datadir}/mender/modules/v3/*"
