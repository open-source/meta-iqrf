#!/bin/sh

case "$0" in
	*_ro*)
		echo "Remount ro"
		mount -o remount,ro /
	;;
	*_rw*)
		echo "Remount rw"
		mount -o remount,rw /
	;;
esac
