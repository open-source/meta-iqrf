LICENSE = "CLOSED"
DESCRIPTION = "Run script which will remount rootfs rw or ro"

SRC_URI = "file://remount_rw.sh \
"

FILES:${PN} += "${sbindir}/*"

do_install() {
	install -d ${D}/usr/sbin
	install -m 755 ${WORKDIR}/remount_rw.sh ${D}${sbindir}
	ln -sf ${sbindir}/remount_rw.sh ${D}${sbindir}/remount_ro.sh
}
