LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/gateway/iqrf-gateway-uploader"
DESCRIPTION = "IQRF Gateway SPI uploader"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3c545a1b819d66eda82219ed1f1d3e25"

inherit cmake systemd

DEPENDS = "boost rapidjson"

SRC_URI = "gitsm://git@gitlab.iqrf.org:2222/gateway/iqrf-gateway-uploader.git;protocol=ssh;branch=release/v1.0.x;tag=v${PV} \
	file://spi-reset-tr.patch \
"

S = "${WORKDIR}/git"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}

do_install:append() {
	install -d ${D}${sysconfdir}/iqrf-gateway-uploader
	install -m 644 ${S}/config/config.json ${D}${sysconfdir}/iqrf-gateway-uploader
}
