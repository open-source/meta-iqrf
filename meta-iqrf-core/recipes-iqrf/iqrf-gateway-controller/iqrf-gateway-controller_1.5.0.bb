LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/gateway/iqrf-gateway-controller"
DESCRIPTION = "IQRF Gateway LED and button controller"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f258b2eebf8a46d07bab9d12ce8f3f26"

inherit cmake systemd

DEPENDS = "boost websocketpp asio"

SRC_URI = "gitsm://git@gitlab.iqrf.org:2222/gateway/iqrf-gateway-controller.git;protocol=ssh;branch=release/v1.5.x;tag=v${PV} \
"

S = "${WORKDIR}/git"

FILES:${PN} += "${systemd_system_unitdir}/*"

SYSTEMD_SERVICE:${PN} = "iqrf-gateway-controller.service iqrf-gateway-controller-wdt.service"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}

do_install:append() {
	install -d ${D}${systemd_unitdir}/system
	install -m 644 ${S}/debian/iqrf-gateway-controller.service ${D}${systemd_unitdir}/system/
	install -m 644 ${S}/install/systemd/iqrf-gateway-controller-wdt.service ${D}${systemd_system_unitdir}

	install -d ${D}${bindir}
	install -d ${D}${sysconfdir}/logrotate.d/
	install -m 644 ${S}/config/iqrf-gateway-controller ${D}${sysconfdir}/logrotate.d/
}

# workaround script use bash but it's not mandatory
INSANE_SKIP += "file-rdeps"
