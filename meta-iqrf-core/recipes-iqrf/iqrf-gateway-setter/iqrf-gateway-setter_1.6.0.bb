LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/gateway/iqrf-gateway-setter"
DESCRIPTION = "IQRF Gateway setup tool"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0da8b2ee53921aaf4628e11569125988"

inherit setuptools3 systemd

RDEPENDS:${PN} = "\
	python3 \
	python3-bcrypt \
	python3-influxdb \
	python3-netifaces \
	python3-sentry-sdk \
	python3-systemd \
	python3-packaging \
	python3-pkg-resources \
	python3-websocket-client \
	python3-certifi \
	python3-urllib3 \
	python3-semver \
	python3-shellescape \
	python3-requests \
"

SRC_URI = "gitsm://git@gitlab.iqrf.org:2222/gateway/iqrf-gateway-setter.git;protocol=ssh;branch=release/v1.6.x;tag=v${PV} \
	file://0002-Webapp-factory-reset-fix.patch \
"

S = "${WORKDIR}/git"

FILES:${PN} += " ${sysconfdir}/* ${systemd_system_unitdir}/* ${datadir}/*"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}

do_install:append() {
	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${S}/debian/iqrf-gateway-setter.service ${D}${systemd_system_unitdir}

	# folder for config backup (for overlay)
	install -d ${D}/usr/share/iqrf-gateway
}

SYSTEMD_SERVICE:${PN} = "iqrf-gateway-setter.service"
