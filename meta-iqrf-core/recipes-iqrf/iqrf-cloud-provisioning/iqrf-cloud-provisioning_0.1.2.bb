LICENSE = "CLOSED"
HOMEPAGE = "https://gitlab.iqrf.org/gateway/iqrf-cloud-provisioning"

inherit cmake systemd
	
DEPENDS = "shape shapeware"
SRCREV_FORMAT = "cloud_iqd-gw0x"

SRC_URI = "gitsm://git@gitlab.iqrf.org:2222/gateway/iqrf-cloud-provisioning.git;protocol=ssh;branch=master;tag=v${PV} \
	file://0001-cmake-updates.patch \
	file://iqrf-cloud-provisioning.service \
"

S = "${WORKDIR}/git"

FILES_${PN} += "${sysconfdir} ${libdir}/iqrf-cloud-provisioning/*"

INSANE_SKIP:${PN}-dev += "dev-elf"

SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_SERVICE:${PN} = "iqrf-cloud-provisioning.service"

CLOUD_BASE_CFG_PATH = "${S}/iqrf-cloud-provisioning"
CLOUD_BASE_CERTS_PATH = "${S}/certs/bootstrap"

do_install[network] = "1"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}

EXTRA_OECMAKE = ""

do_install:append() {
	# install systemd
	install -d ${D}${systemd_unitdir}/system
	install -m 644 ${WORKDIR}/iqrf-cloud-provisioning.service ${D}${systemd_unitdir}/system/

	# install config directories and files
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/iqrf-cloud-provisioning
	cp -r ${CLOUD_BASE_CFG_PATH}/configuration/* ${D}${sysconfdir}/iqrf-cloud-provisioning
	cp -r ${CLOUD_BASE_CFG_PATH}/configurationLin/configuration/* ${D}${sysconfdir}/iqrf-cloud-provisioning
	cp -r ${CLOUD_BASE_CFG_PATH}/configurationLin/config.json ${D}${sysconfdir}/iqrf-cloud-provisioning

	# install certs directories and files
	install -d ${D}/usr/share/iqrf-cloud-provisioning/cert
	install -d ${D}/usr/share/iqrf-cloud-provisioning/cert/bootstrap
	cp -r ${CLOUD_BASE_CERTS_PATH}/* ${D}/usr/share/iqrf-cloud-provisioning/cert/bootstrap

	# install libraries	
	install -d ${D}${libdir}/iqrf-cloud-provisioning
	LIBS=`find ${B}/bin -type f -name "*.so"`
	for l in $LIBS; do
		bbwarn "${l}"
		cp ${l} ${D}${libdir}/iqrf-cloud-provisioning
	done

	rm -rf ${D}/usr/runcfg
}
