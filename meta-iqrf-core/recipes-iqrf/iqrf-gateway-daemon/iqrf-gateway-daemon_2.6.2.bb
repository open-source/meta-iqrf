LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon"
LIC_FILES_CHKSUM = "file://LICENSE;md5=68dc8d3532fce915690a10f965325944"

inherit cmake systemd python3native

DEPENDS = "shape shapeware python3-native python3-requests-native libzip"
RDEPEND_${PN} += "shape shapeware"

SRCREV_FORMAT = "daemon_iqd-gw-01"

SRC_URI = "gitsm://gitlab.iqrf.org/open-source/iqrf-gateway-daemon.git;protocol=https;branch=release/v2.6.x;tag=v${PV} \
	file://0001-cmake-updates.patch \
	file://0002-cache-update-cmake.patch \
	file://iqrf-gateway-daemon.service \
	file://iqrf__JsCache.json \
	file://iqrf__MqttMessaging.json \
	file://update-cache.py \
"

S = "${WORKDIR}/git"

FILES_${PN} += "${sysconfdir} ${libdir}/iqrf-gateway-daemon/*"

INSANE_SKIP:${PN}-dev += "dev-elf"

SYSTEMD_SERVICE:${PN} = "iqrf-gateway-daemon.service"

IQRF_BASE_CFG_PATH = "${S}/src/start-IqrfDaemon/"

EXTRA_OECMAKE:append = " -DDAEMON_VERSION=v2.6.2"

do_install[network] = "1"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}

do_install:append() {
	# install systemd
	install -d ${D}${systemd_unitdir}/system
	install -m 644 ${WORKDIR}/iqrf-gateway-daemon.service ${D}${systemd_unitdir}/system/

	# install extra directories
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/iqrf-gateway-daemon

	cp -r ${IQRF_BASE_CFG_PATH}/configuration/* ${D}${sysconfdir}/iqrf-gateway-daemon/
	cp -r ${IQRF_BASE_CFG_PATH}/configuration-LinDeploy/* ${D}${sysconfdir}/iqrf-gateway-daemon

	install -m 644 ${WORKDIR}/iqrf__IdeCounterpart.json ${D}${sysconfdir}/iqrf-gateway-daemon
	install -m 644 ${WORKDIR}/iqrf__JsCache.json ${D}${sysconfdir}/iqrf-gateway-daemon
	install -m 644 ${WORKDIR}/iqrf__MqttMessaging.json ${D}${sysconfdir}/iqrf-gateway-daemon

	install -d ${D}${sysconfdir}/iqrf-gateway-daemon/certs
	install -d ${D}${sysconfdir}/iqrf-gateway-daemon/certs/core

	install -d ${D}/var/cache/iqrf-gateway-daemon
	install -d ${D}/var/cache/iqrf-gateway-daemon/scheduler
	install -d ${D}/var/cache/iqrf-gateway-daemon/upload

	# run update cache
	install -d ${IQRF_BASE_CFG_PATH}/iqrfRepoCache
	install -m 644 ${WORKDIR}/update-cache.py ${IQRF_BASE_CFG_PATH}/iqrfRepoCache
	python3 ${IQRF_BASE_CFG_PATH}/iqrfRepoCache/update-cache.py
	cp -r ${IQRF_BASE_CFG_PATH}/iqrfRepoCache ${D}/var/cache/iqrf-gateway-daemon/
	cp ${IQRF_BASE_CFG_PATH}/iqrfRepoCache/cache/server/data.json ${D}/var/cache/iqrf-gateway-daemon/iqrfRepoCache/serverCheck.json

	install -d ${D}/usr/share/iqrf-gateway-daemon
	install -d ${D}/usr/share/iqrf-gateway-daemon/apiSchemas
	cp -r ${S}/api/*.json ${D}/usr/share/iqrf-gateway-daemon/apiSchemas

	cp -r ${IQRF_BASE_CFG_PATH}/cfgSchemas ${D}/usr/share/iqrf-gateway-daemon
	cp -r ${IQRF_BASE_CFG_PATH}/schedulerSchemas ${D}/usr/share/iqrf-gateway-daemon

	install -d ${D}/usr/share/iqrf-gateway-daemon/cacheSchemas
	cp -r ${S}/src/cache-update/schemas/* ${D}/usr/share/iqrf-gateway-daemon/cacheSchemas

	cp -r ${IQRF_BASE_CFG_PATH}/javaScript ${D}/usr/share/iqrf-gateway-daemon
	cp -r ${IQRF_BASE_CFG_PATH}/DB ${D}/usr/share/iqrf-gateway-daemon/

	install -d ${D}${libdir}/iqrf-gateway-daemon

	LIBS=`find ${B}/bin -type f -name "*.so"`
	for l in $LIBS; do
		bbwarn "${l}"
		cp ${l} ${D}${libdir}/iqrf-gateway-daemon
	done

	rm -rf ${D}/usr/runcfg
}
