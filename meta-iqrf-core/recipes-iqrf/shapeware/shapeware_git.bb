LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://README.md;md5=08306b43065ed45a35abdac57eb6f560"

inherit cmake

SRCREV = "096ac788c49473baa7766432b30db9d75c5d97e3"
#PV = "1.3.0+git${SRCPV}"

DEPENDS = "shape websocketpp curl boost zeromq asio paho-mqtt-c"

SRC_URI = "git://gitlab.iqrf.org/open-source/iqrf-gateway-daemon-utils/shapeware;protocol=https;branch=master \
	file://0001-cmake-updates.patch \
	file://0002-cmake-enable-aws.patch \
"

S = "${WORKDIR}/git"

FILES:${PN} += "/usr/lib/*"

do_install:append() {
	install -d ${D}${libdir}/iqrf-gateway-daemon
	install -d ${D}${libdir}/iqrf-cloud-provisioning

	SHAPELIBS=`find ${B}/bin -type f -name "*.so"`
        for f in $SHAPELIBS; do
	        cp $f ${D}${libdir}/iqrf-gateway-daemon/
			cp $f ${D}${libdir}/iqrf-cloud-provisioning/
		rm -rf ${D}/{libdir}/basename $f
        done
	
	rm ${D}${bindir}/startup
	rm -rf ${D}${bindir}
	rm -rf ${D}/usr/runcfg
}

INSANE_SKIP:${PN}-dev += "dev-elf"
