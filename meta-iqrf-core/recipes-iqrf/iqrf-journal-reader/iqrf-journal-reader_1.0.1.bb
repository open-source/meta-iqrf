LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/open-source/iqrf-journal-reader"
DESCRIPTION = "IQRF Journal Reader utility"
LIC_FILES_CHKSUM = "file://LICENSE;md5=04831be386832ddcb36078c78c8fbad0"

inherit cmake systemd

DEPENDS = "boost nlohmann-json systemd"

SRC_URI = "gitsm://gitlab.iqrf.org/open-source/iqrf-journal-reader.git;protocol=https;branch=release/v1.0.x;tag=v${PV}"

S = "${WORKDIR}/git"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}
