#!/bin/sh

dir=/data/translator
file=/data/translator/bootpart

if [[ ! -e "$dir" ]]; then
    mkdir -p "$dir"
    touch "$file"
elif [[ ! -d "$dir" ]]; then
    echo "$dir already exists but is not a directory, exit" 1>&2
    exit 1
elif [[ -d "$dir" ]]; then
    echo "$dir already exists" 1>&2
    if [[ -f "$file" ]]; then
        echo "$file exists, exit" 1>&2
        exit 1
    fi
fi

currentKey=$(jq '.rest.api_key' /etc/iqrf-gateway-translator/config.json)
  
if [ "$currentKey" = '""' ]; then
    key=$(sh -c '/usr/sbin/iqrf-gateway-webapp-manager api-key:add --no-interaction -d "iqrfgt" --no-formatting | tr -d "[:space:]"')
    jq --arg key "$key" '.rest.api_key = $key' /etc/iqrf-gateway-translator/config.json > /data/translator/tmp.json && mv /data/translator/tmp.json /etc/iqrf-gateway-translator/config.json
fi

if [ -f /etc/iqrf-gateway.json ]; then
    gwId=$(jq -r '.gwId' /etc/iqrf-gateway.json)
    jq --arg gwId "$gwId" '.mqtt.cid = $gwId | .mqtt.request_topic |= (sub("ffffffffffffffff"; $gwId)) | .mqtt.response_topic |= (sub("ffffffffffffffff"; $gwId))' /etc/iqrf-gateway-translator/config.json > /data/translator/tmp.json && mv /data/translator/tmp.json /etc/iqrf-gateway-translator/config.json
else
    gwId=$(cat /dev/urandom | tr -dc 'A-F0-9' | fold -w 16 | head -n 1)
    jq --arg gwId "$gwId" '.mqtt.cid = $gwId' /etc/iqrf-gateway-translator/config.json > /data/translator/tmp.json && mv /data/translator/tmp.json /etc/iqrf-gateway-translator/config.json
fi
