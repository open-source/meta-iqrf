LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/open-source/iqrf-gateway-translator"
DESCRIPTION = "IQRF Gateway MQTT-REST translator"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

RDEPENDS:${PN} = "python3 python3-jsonschema python3-paho-mqtt python3-requests"

inherit systemd setuptools3

SRCREV = "${AUTOREV}"

SRC_URI = "git://git@gitlab.iqrf.org:2222/open-source/iqrf-gateway-translator.git;protocol=ssh;branch=master \
	file://setup_translator.sh \
	file://iqrf-gateway-translator-setup.service \
"

SYSTEMD_SERVICE:${PN} = "iqrf-gateway-translator-setup.service"

S = "${WORKDIR}/git"

FILES:${PN} += " ${sysconfdir}/* ${systemd_system_unitdir}/*"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install:append() {
	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${S}/debian/iqrf-gateway-translator.service ${D}${systemd_system_unitdir}
	install -m 644 ${WORKDIR}/iqrf-gateway-translator-setup.service ${D}${systemd_system_unitdir}

	install -d ${D}${sysconfdir}/iqrf-gateway-translator
	install -m 644 ${S}/config/config.json ${D}${sysconfdir}/iqrf-gateway-translator

	install -d ${D}${sbindir}
	install -m 755 ${WORKDIR}/setup_translator.sh ${D}${sbindir}
}
