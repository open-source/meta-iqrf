LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e3fc50a88d0a364313df4b21ef20c29e"

inherit cmake

DEPENDS = "python3-native"

# Modify these as desired
#PV = "1.1.0+git${SRCPV}"
SRCREV = "461715555e6961bf011e06a0c715ae607a7ca74f"

SRC_URI = "gitsm://gitlab.iqrf.org/open-source/iqrf-gateway-daemon-utils/shape.git;protocol=https;branch=master \
	file://0001-cmakelists.patch \
    file://0002-shape-component-declaration.patch \
	file://0003-Update-rapidjson-to-8f4c021fa2f1e001d2376095928fc053.patch \
	file://0004-Patch-enable-debug-tracing-in-release-build.patch \
"

S = "${WORKDIR}/git"

FILES:${PN} += "/usr/lib/*"
FILES:${PN}-dev = "/usr/include/shape/*"

# hack to copy library to proper location
do_install:append() {
	install -d ${D}${libdir}/iqrf-gateway-daemon
	install -d ${D}${libdir}/iqrf-cloud-provisioning
	install -m 755 ${B}/bin/libTraceFileService.so ${D}${libdir}/iqrf-gateway-daemon
	install -m 755 ${B}/bin/libTraceFileService.so ${D}${libdir}/iqrf-cloud-provisioning
	install -m 755 ${B}/bin/libTraceFormatService.so ${D}${libdir}/iqrf-gateway-daemon
	install -m 755 ${B}/bin/libTraceFormatService.so ${D}${libdir}/iqrf-cloud-provisioning

	rm ${D}${bindir}/startup
	rm -rf ${D}${bindir}
	rm -rf ${D}/usr/runcfg
}

INSANE_SKIP:${PN}-dev += "dev-elf"
