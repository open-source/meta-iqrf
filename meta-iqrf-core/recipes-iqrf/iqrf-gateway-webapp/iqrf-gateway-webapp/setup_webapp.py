#!/usr/bin/python3

import subprocess
import os

def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

def main():
    if os.path.exists("/data/webapp") != True:
        os.mkdir("/data/webapp")

    # first run -> create empty file
    if os.path.exists("/data/webapp/bootpart") != True:
        touch("/data/webapp/bootpart")

    try:
        cmd = subprocess.run(["fw_printenv", "mender_boot_part"], check=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)
    else:
        out = cmd.stdout.decode('utf-8').strip().split("=")
        print("Command output (split + strip):",out)
        if len(out) > 1 and out[1] != '':
            part = out[1]
        else:
            part = "0"

        f = open("/data/webapp/bootpart", "r+")
        part_file = f.readline().strip("\n")

        print("file partition:%s - system:%s" % (part_file, part))
        if part_file == "" or int(part_file) != int(part):
            if part_file == "" and os.path.exists("/data/webapp/db_created") != True:
                # first boot create database
                print("Create database")
                os.system("/usr/sbin/iqrf-gateway-webapp-manager database:create")

            print("Patch database")
            os.system("/usr/sbin/iqrf-gateway-webapp-manager migrations:migrate --no-interaction")
            os.system("/usr/sbin/iqrf-gateway-webapp-manager doctrine:fixtures:load --append --no-interaction")
            os.system("/usr/sbin/iqrf-gateway-webapp-manager iqrf-os:import-patches")

            # overwrite boot part
            f.seek(0)
            f.write(part)
            f.close()
        else:
            print("Database initialized already")


if __name__ == "__main__":
    main()
