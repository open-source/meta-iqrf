LICENSE = "Apache-2.0"
HOMEPAGE = "https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp"
LIC_FILES_CHKSUM = "file://LICENSE;md5=c5213082619b237ff5202a01edacc0a0"

DEPENDS += "composer-native php-native openssl-native nodejs-native"
RDEPENDS:${PN} += "nginx php php-cgi php-cli php-fpm php-opcache php-phar python3-core"

SRC_URI = "gitsm://gitlab.iqrf.org/open-source/iqrf-gateway-webapp.git;protocol=https;branch=release/v2.6.x;tag=v${PV} \
	file://0001-app-config-Disable-sudo.patch \
	file://0002-Makefile-fixes.patch \
	file://0003-app-config-update-timesync-path.patch \
	file://iqrf-gateway-webapp-setup.service \
	file://setup_webapp.py \
"

S = "${WORKDIR}/git"

do_compile[network] = "1"

python() {
	bb.warn("SRCPV:%s"%d.getVar("SRCPV"))
}

do_install() {
	export DESTDIR="${D}"
	oe_runmake install

	install -d ${D}${sbindir}
	ln -sf /usr/share/iqrf-gateway-webapp/bin/manager ${D}${sbindir}/iqrf-gateway-webapp-manager

	WEBAPP_CERT_DIR="${D}${sysconfdir}/iqrf-gateway-webapp/certs"

	# generate certificate
	if [ ! -f "${WEBAPP_CERT_DIR}/cert.pem" ] || [ ! -f "${WEBAPP_CERT_DIR}/privkey.pem" ] ; then
		openssl ecparam -name secp384r1 -genkey -param_enc named_curve -out ${WEBAPP_CERT_DIR}/privkey.pem
		openssl req -new -x509 -sha256 -nodes -days 3650 \
			-subj "/CN=IQRF Gateway/C=CZ/ST=Hradec Kralove Region/L=Jicin/O=IQRF Tech s.r.o." \
			-key ${WEBAPP_CERT_DIR}/privkey.pem -out ${WEBAPP_CERT_DIR}/cert.pem
		chmod 600 ${WEBAPP_CERT_DIR}/*.pem
	fi

	if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
		install -d ${D}${sysconfdir}/tmpfiles.d
		echo "d ${localstatedir}/log/iqrf-gateway-webapp - - - -" \
			> ${D}${sysconfdir}/tmpfiles.d/iqrf-gateway-webapp.conf

		# install systemd setup service
		install -d ${D}${systemd_unitdir}/system
		install -m 644 ${WORKDIR}/iqrf-gateway-webapp-setup.service ${D}${systemd_unitdir}/system/
	fi

	install -d ${D}${sbindir}
	install -m 755 ${WORKDIR}/setup_webapp.py ${D}${sbindir}
}

FILES:${PN} += "${sbindir}/* ${sysconfdir}/tmpfiles.d/* ${systemd_system_unitdir}/*"

inherit systemd

SYSTEMD_SERVICE:${PN} = "iqrf-gateway-webapp-setup.service"

INSANE_SKIP:${PN}:append = " empty-dirs"
