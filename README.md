# meta-iqrf

Yocto layer for IQRF Gateway software.

## Acknowledgement

This project has been made possible with a government grant by means of [the Ministry of Industry and Trade of the Czech Republic](https://www.mpo.cz/) in [the TRIO program](https://starfos.tacr.cz/cs/project/FV40132).
