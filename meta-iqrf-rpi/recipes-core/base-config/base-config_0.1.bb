DESCRIPTION = "Basic configuration for IQRF images"
LICENSE = "CLOSED"

SRC_URI = "file://hosts \
	file://hostname \
	file://iqrf-gateway.json \
"

S = "${WORKDIR}"

do_install() {
	install -d ${D}${sysconfdir}
	install -m 0644 ${WORKDIR}/hosts    ${D}${sysconfdir}/hosts
	install -m 0644 ${WORKDIR}/hostname ${D}${sysconfdir}/hostname
	install -m 0644 ${WORKDIR}/iqrf-gateway.json ${D}${sysconfdir}
}

FILES:${PN} += "${sysconfdir}"
