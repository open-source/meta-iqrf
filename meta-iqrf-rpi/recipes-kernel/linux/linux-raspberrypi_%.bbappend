FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://0001-overlay-Add-gpio-watchdog-overlay.patch \
	file://docker.cfg \
	file://g_serial.cfg \
	file://wifi_usb.cfg \
	file://gpio_wd.cfg \
	file://rtc.cfg \
	file://usb-kbd.cfg \
	file://usb_lte.cfg \
	file://sysfs_gpio.cfg \
	file://wireguard.cfg \
	file://audit.cfg \
"

CMDLINE:append = " console=serial0,115200N8 loglevel=7 fbcon=map:10 fbcon=font:VGA10x10"

KERNEL_MODULE_AUTOLOAD += "g_serial"
