FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://lte.nmconnection \
"

FILES:${PN} += "${sysconfdir}/*"

do_install:append() {
	install -d ${D}${sysconfdir}/NetworkManager/system-connections
	install -m 600 ${WORKDIR}/lte.nmconnection ${D}${sysconfdir}/NetworkManager/system-connections
}
