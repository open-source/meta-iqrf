FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://ModemManager.service \
"

FILES:${PN} += "${systemd_system_unitdir}/*"

do_install:append() {
	#custom service with lte reset
	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${WORKDIR}/ModemManager.service ${D}${systemd_system_unitdir}
}
