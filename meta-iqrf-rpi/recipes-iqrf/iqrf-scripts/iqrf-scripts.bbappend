FILESEXTRAPATHS:prepend := "${THISDIR}/iqrf-scripts:"

SRC_URI += "file://lte_reset.sh \
	    	file://lte.rules \
			file://mm-reset \
			file://hwrtc.service \
"

FILES:${PN} += "${systemd_system_unitdir}/*\
				${sysconfdir}/*\
"

do_install:append() {
	#RESET
	install -m 755 ${WORKDIR}/lte_reset.sh ${D}${sbindir}

	#CRON
	install -d ${D}${sysconfdir}/cron.daily
	install -m 755 ${WORKDIR}/mm-reset ${D}${sysconfdir}/cron.daily/mm-reset

	#UDEV
	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 666 ${WORKDIR}/lte.rules ${D}${sysconfdir}/udev/rules.d/lte.rules

	#RTC
	install -d ${D}${systemd_system_unitdir}
	install -m 644 ${WORKDIR}/hwrtc.service ${D}${systemd_system_unitdir}/hwrtc.service
	# manually enable service
    install -d ${D}${sysconfdir}/systemd/system/sysinit.target.wants
    ln -sf ${systemd_unitdir}/system/hwrtc.service \
        ${D}${sysconfdir}/systemd/system/sysinit.target.wants/hwrtc.service
}
