#!/bin/sh

#
# Generate reset for LTE modul via pin 26
#

PIN=26

if [ ! -d /sys/class/gpio/gpio${PIN} ]; then
	echo ${PIN} > /sys/class/gpio/export
	echo out > /sys/class/gpio/gpio${PIN}/direction
fi

echo 1 > /sys/class/gpio/gpio${PIN}/value
echo 0 > /sys/class/gpio/gpio${PIN}/value
sleep 1
echo 1 > /sys/class/gpio/gpio${PIN}/value
sleep 1

# Remove lock file
rm -f /var/lock/LCK..ttyAMA2