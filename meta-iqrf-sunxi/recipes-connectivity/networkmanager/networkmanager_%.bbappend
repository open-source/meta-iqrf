FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://10-override-wifi-random-mac-disable.conf \
			file://20-override-wifi-powersave-disable.conf \
"

FILES:${PN} += "${sysconfdir}/*"

do_install:append() {
	install -d ${D}${sysconfdir}/NetworkManager/conf.d
	install -m 600 ${WORKDIR}/10-override-wifi-random-mac-disable.conf ${D}${sysconfdir}/NetworkManager/conf.d
	install -m 600 ${WORKDIR}/20-override-wifi-powersave-disable.conf ${D}${sysconfdir}/NetworkManager/conf.d
}
