FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://config.json \
	file://iqrf__IqrfSpi.json \
	file://iqrf__IqrfUart.json \
	file://iqrf__IdeCounterpart.json \
"

do_install:append() {
	install -m 644 ${WORKDIR}/config.json ${D}${sysconfdir}/iqrf-gateway-daemon
	install -m 644 ${WORKDIR}/iqrf__IqrfSpi.json ${D}${sysconfdir}/iqrf-gateway-daemon
	install -m 644 ${WORKDIR}/iqrf__IqrfUart.json ${D}${sysconfdir}/iqrf-gateway-daemon
	install -m 644 ${WORKDIR}/iqrf__IdeCounterpart.json ${D}${sysconfdir}/iqrf-gateway-daemon
}
