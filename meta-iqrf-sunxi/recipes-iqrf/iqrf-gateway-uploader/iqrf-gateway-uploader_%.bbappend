FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://config.json \
"

do_install:append() {
	install -m 644 ${WORKDIR}/config.json ${D}${sysconfdir}/iqrf-gateway-uploader
}
