FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " file://spidev.cfg \
	file://usb_lte.cfg \
	file://g_serial.cfg \
	file://wifi_usb.cfg \
	file://gpio_wd.cfg \
	file://docker.cfg \
	file://sysfs_gpio.cfg \
	file://wireguard.cfg \
	file://0001-sun8i-h2-orangepi-zero-Enable-spidev-uart1-uart2.patch \
	file://0002-dts-Enable-uart1-nad-uart2.patch \
	file://0003-arm-boot-dts-sun8i-h2-plus-orangepi-zero-Added-suppo.patch \
	file://0004-dts-Enable-always-running-flag-for-gpio-watchdog.patch \
	file://0005-dts-set-dual-role-usb-to-host-mode.patch \
"

KERNEL_MODULE_AUTOLOAD += "g_serial"
